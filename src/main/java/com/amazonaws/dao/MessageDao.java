/*
 * Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.amazonaws.dao;

import com.amazonaws.exception.CouldNotCreateMessageException;
import com.amazonaws.exception.MessageDoesNotExistException;
import com.amazonaws.exception.TableDoesNotExistException;
import com.amazonaws.exception.UnableToDeleteException;
import com.amazonaws.exception.UnableToUpdateException;
import com.amazonaws.model.Message;
import com.amazonaws.model.MessagePage;
import com.amazonaws.model.request.CreateMessageRequest;

import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.ConditionalCheckFailedException;
import software.amazon.awssdk.services.dynamodb.model.DeleteItemRequest;
import software.amazon.awssdk.services.dynamodb.model.DeleteItemResponse;
import software.amazon.awssdk.services.dynamodb.model.GetItemRequest;
import software.amazon.awssdk.services.dynamodb.model.GetItemResponse;
import software.amazon.awssdk.services.dynamodb.model.PutItemRequest;
import software.amazon.awssdk.services.dynamodb.model.ResourceNotFoundException;
import software.amazon.awssdk.services.dynamodb.model.ReturnValue;
import software.amazon.awssdk.services.dynamodb.model.ScanRequest;
import software.amazon.awssdk.services.dynamodb.model.ScanResponse;
import software.amazon.awssdk.services.dynamodb.model.UpdateItemRequest;
import software.amazon.awssdk.services.dynamodb.model.UpdateItemResponse;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class MessageDao {

    private static final String UPDATE_EXPRESSION
            = "SET name = :name, email = :email, message = :message ADD version :o";
    private static final String MESSAGE_ID = "messageId";
    private static final String EMAIL_WAS_NULL = "email was null";
    private static final String MESSAGE_WAS_NULL = "message was null";
    private static final String VERSION_WAS_NULL = "version was null";

    private final String tableName;
    private final DynamoDbClient dynamoDb;
    private final int pageSize;

    /**
     * Constructs an MessageDao.
     * @param dynamoDb dynamodb client
     * @param tableName name of table to use for messages
     * @param pageSize size of pages for getMessages
     */
    public MessageDao(final DynamoDbClient dynamoDb, final String tableName,
                    final int pageSize) {
        this.dynamoDb = dynamoDb;
        this.tableName = tableName;
        this.pageSize = pageSize;
    }

    /**
     * Returns an message or throws if the message does not exist.
     * @param messageId id of message to get
     * @return the message if it exists
     * @throws MessageDoesNotExistException if the message does not exist
     */
    public Message getMessage(final String messageId) {
        try {
            return Optional.ofNullable(
                    dynamoDb.getItem(GetItemRequest.builder()
                            .tableName(tableName)
                            .key(Collections.singletonMap(MESSAGE_ID,
                                    AttributeValue.builder().s(messageId).build()))
                            .build()))
                    .map(GetItemResponse::item)
                    .map(this::convert)
                    .orElseThrow(() -> new MessageDoesNotExistException("Message "
                            + messageId + " does not exist"));
        } catch (ResourceNotFoundException e) {
            throw new TableDoesNotExistException("Message table " + tableName + " does not exist");
        }
    }

    /**
     * Gets a page of messages, at most pageSize long.
     * @param exclusiveStartMessageId the exclusive start id for the next page.
     * @return a page of messages.
     * @throws TableDoesNotExistException if the message table does not exist
     */
    public MessagePage getMessages(final String exclusiveStartMessageId) {
        final ScanResponse result;

        try {
            ScanRequest.Builder scanBuilder = ScanRequest.builder()
                    .tableName(tableName)
                    .limit(pageSize);
            if (!isNullOrEmpty(exclusiveStartMessageId)) {
                scanBuilder.exclusiveStartKey(Collections.singletonMap(MESSAGE_ID,
                        AttributeValue.builder().s(exclusiveStartMessageId).build()));
            }
            result = dynamoDb.scan(scanBuilder.build());
        } catch (ResourceNotFoundException e) {
            throw new TableDoesNotExistException("Message table " + tableName
                    + " does not exist");
        }

        final List<Message> messages = result.items().stream()
                .map(this::convert)
                .collect(Collectors.toList());

        MessagePage.MessagePageBuilder builder = MessagePage.builder().messages(messages);
        if (result.lastEvaluatedKey() != null && !result.lastEvaluatedKey().isEmpty()) {
            if ((!result.lastEvaluatedKey().containsKey(MESSAGE_ID)
                    || isNullOrEmpty(result.lastEvaluatedKey().get(MESSAGE_ID).s()))) {
                throw new IllegalStateException(
                    "messageId did not exist or was not a non-empty string in the lastEvaluatedKey");
            } else {
                builder.lastEvaluatedKey(result.lastEvaluatedKey().get(MESSAGE_ID).s());
            }
        }

        return builder.build();
    }

    /**
     * Updates an message object.
     * @param message message to update
     * @return updated message
     */
    public Message updateMessage(final Message message) {
        if (message == null) {
            throw new IllegalArgumentException("Message to update was null");
        }
        String messageId = message.getMessageId();
        if (isNullOrEmpty(messageId)) {
            throw new IllegalArgumentException("messageId was null or empty");
        }
        Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();
        expressionAttributeValues.put(":name",
                AttributeValue.builder().s(validateName(message.getName())).build());
        expressionAttributeValues.put(":email",
                AttributeValue.builder().s(message.getEmail()).build());

        expressionAttributeValues.put(":message",
                AttributeValue.builder().s(message.getMessage()).build());

        expressionAttributeValues.put(":o", AttributeValue.builder().n("1").build());
        try {
            expressionAttributeValues.put(":v",
                AttributeValue.builder().n(message.getVersion().toString()).build());
        } catch (NullPointerException e) {
            throw new IllegalArgumentException(VERSION_WAS_NULL);
        }
        final UpdateItemResponse result;
        try {
            result = dynamoDb.updateItem(UpdateItemRequest.builder()
                    .tableName(tableName)
                    .key(Collections.singletonMap(MESSAGE_ID,
                            AttributeValue.builder().s(message.getMessageId()).build()))
                    .returnValues(ReturnValue.ALL_NEW)
                    .updateExpression(UPDATE_EXPRESSION)
                    .conditionExpression("attribute_exists(messageId) AND version = :v")
                    .expressionAttributeValues(expressionAttributeValues)
                    .build());
        } catch (ConditionalCheckFailedException e) {
            throw new UnableToUpdateException(
                    "Either the message did not exist or the provided version was not current");
        } catch (ResourceNotFoundException e) {
            throw new TableDoesNotExistException("Message table " + tableName
                    + " does not exist and was deleted after reading the message");
        }
        return convert(result.attributes());
    }

    /**
     * Deletes an message.
     * @param messageId message id of message to delete
     * @return the deleted message
     */
    public Message deleteMessage(final String messageId) {
        final DeleteItemResponse result;
        try {
            return Optional.ofNullable(dynamoDb.deleteItem(DeleteItemRequest.builder()
                            .tableName(tableName)
                            .key(Collections.singletonMap(MESSAGE_ID,
                                    AttributeValue.builder().s(messageId).build()))
                            .conditionExpression("attribute_exists(messageId)")
                            .returnValues(ReturnValue.ALL_OLD)
                            .build()))
                    .map(DeleteItemResponse::attributes)
                    .map(this::convert)
                    .orElseThrow(() -> new IllegalStateException(
                            "Condition passed but deleted item was null"));
        } catch (ConditionalCheckFailedException e) {
            throw new UnableToDeleteException(
                    "A competing request changed the message while processing this request");
        } catch (ResourceNotFoundException e) {
            throw new TableDoesNotExistException("Message table " + tableName
                    + " does not exist and was deleted after reading the message");
        }
    }

    private Message convert(final Map<String, AttributeValue> item) {
        if (item == null || item.isEmpty()) {
            return null;
        }
        Message.MessageBuilder builder = Message.builder();

        try {
            builder.messageId(item.get(MESSAGE_ID).s());
        } catch (NullPointerException e) {
            throw new IllegalStateException(
                    "item did not have an messageId attribute or it was not a String");
        }

        try {
            builder.name(item.get("name").s());
        } catch (NullPointerException e) {
            throw new IllegalStateException(
                    "item did not have an name attribute or it was not a String");
        }

        try {
            builder.email(item.get("email").s());
        } catch (NullPointerException e) {
            throw new IllegalStateException(
                    "item did not have an email attribute or it was not a String");
        }

        try {
            builder.message(item.get("message").s());
        } catch (NullPointerException e) {
            throw new IllegalStateException(
                    "item did not have an message attribute or it was not a String");
        }

        try {
            builder.version(Long.valueOf(item.get("version").n()));
        } catch (NullPointerException | NumberFormatException e) {
            throw new IllegalStateException(
                    "item did not have an version attribute or it was not a Number");
        }

        return builder.build();
    }

    private Map<String, AttributeValue> createMessageItem(final CreateMessageRequest message) {
        Map<String, AttributeValue> item = new HashMap<>();
        item.put(MESSAGE_ID, AttributeValue.builder().s(UUID.randomUUID().toString()).build());
        item.put("version", AttributeValue.builder().n("1").build());
        item.put("name",
                AttributeValue.builder().s(validateName(message.getName())).build());
        item.put("email",
                AttributeValue.builder().s(message.getEmail()).build());
        item.put("message",
                AttributeValue.builder().s(message.getMessage()).build());

        return item;
    }

    private String validateName(final String name) {
        if (isNullOrEmpty(name)) {
            throw new IllegalArgumentException("name was null or empty");
        }
        return name;
    }

    /**
     * Creates an message.
     * @param createMessageRequest details of message to create
     * @return created message
     */
    public Message createMessage(final CreateMessageRequest createMessageRequest) {
        if (createMessageRequest == null) {
            throw new IllegalArgumentException("CreateMessageRequest was null");
        }
        int tries = 0;
        while (tries < 10) {
            try {
                Map<String, AttributeValue> item = createMessageItem(createMessageRequest);
                dynamoDb.putItem(PutItemRequest.builder()
                        .tableName(tableName)
                        .item(item)
                        .conditionExpression("attribute_not_exists(messageId)")
                        .build());
                return Message.builder()
                        .messageId(item.get(MESSAGE_ID).s())
                        .name(item.get("name").s())
                        .email(item.get("email").s())
                        .message(item.get("message").s())
                        .version(Long.valueOf(item.get("version").n()))
                        .build();
            } catch (ConditionalCheckFailedException e) {
                tries++;
            } catch (ResourceNotFoundException e) {
                throw new TableDoesNotExistException(
                        "Message table " + tableName + " does not exist");
            }
        }
        throw new CouldNotCreateMessageException(
                "Unable to generate unique message id after 10 tries");
    }

    private static boolean isNullOrEmpty(final String string) {
        return string == null || string.isEmpty();
    }
}

