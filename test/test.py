from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

options = webdriver.ChromeOptions()
URL = 'http://selenium__standalone-chrome:4444/wd/hub'
options.add_argument('--headless')
#options.add_argument("--no-sandbox")
#options.add_argument("--disable-extensions")
#options.add_argument("--disable-gpu")
#options.add_argument("--remote-debugin-port=9222")
#options.add_argument("--screen-size=1920x1080")
options.add_argument("--start-maximized")
options.add_argument("--disable-dev-shm-usage")

driver = webdriver.Remote(command_executor=URL,
                          desired_capabilities=options.to_capabilities())

#driver.set_window_size(1920, 1080)

required_width = driver.execute_script('return document.body.parentNode.scrollWidth')
required_height = driver.execute_script('return document.body.parentNode.scrollHeight')

driver.set_window_size(required_width, required_height)


# driver = webdriver.Remote(
#    command_executor="http://selenium__standalone-chrome:4444/wd/hub",
#    desired_capabilities=DesiredCapabilities.CHROME
#)

driver.get("http://bucket-spring-demo-page.s3-website.us-east-2.amazonaws.com/")
#driver.get("https://www.google.com.hk/")
#driver.save_screenshot('data/screentshots.png')  # has scrollbar
driver.find_element_by_tag_name('body').screenshot('test/result/screentshots_load_page.png')  # avoids scrollbar

element = WebDriverWait(driver, 10).until(
	EC.presence_of_element_located((By.ID, "name_input"))
)
element.send_keys("John Chan")

element = WebDriverWait(driver, 10).until(
	EC.presence_of_element_located((By.ID, "email_input"))
)
element.send_keys("test@example.com")

element = WebDriverWait(driver, 10).until(
	EC.presence_of_element_located((By.ID, "message_input"))
)
element.send_keys("Test Message")

driver.find_element_by_tag_name('body').screenshot('test/result/screentshots_fill_data.png')

element = WebDriverWait(driver, 10).until(
	EC.presence_of_element_located((By.ID, "submitForm"))
)
element.click()

WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "submitResult2")))
driver.find_element_by_tag_name('body').screenshot('test/result/screentshots_after_submit.png')

element = WebDriverWait(driver, 10).until(
	EC.presence_of_element_located((By.ID, "showMsg"))
)
element.click()

WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "listdata2")))
driver.find_element_by_tag_name('body').screenshot('test/result/screentshots_request_data.png')

print(driver.title)
driver.close()
